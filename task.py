import cgi
import json
import re
from collections import OrderedDict


def html_wrapper(content, tag):
    props_map = {
        '.': 'class',
        '#': 'id'
    }
    pattern = re.compile('([{0}])?([^{0}]+)'.format(''.join(props_map.keys())))
    props = pattern.findall(tag)
    props_html = {}
    for prop, value in props[1:]:
        if prop not in props_html:
            props_html[prop] = []
        props_html[prop].append(value)
    props_list = ['%s="%s"' % (props_map[symbol], ' '.join(values)) for symbol, values in props_html.items()]
    return "<{0}>{2}</{1}>".format(
        " ".join([props[0][1], " ".join(props_list)]).rstrip(),
        props[0][1],
        content
    )


def parse_dict(obj_dict):
    result = ""
    for tag, content in obj_dict.items():
        result += html_wrapper(parse_json(content), tag)
    return result


def parse_list(obj_list):
    result = ""
    for item in obj_list:
        temp = ""
        for tag, content in item.items():
            temp += html_wrapper(parse_json(content), tag)
        result += html_wrapper(temp, 'li')
    return html_wrapper(result, 'ul')


def parse_json(obj):
    if isinstance(obj, list):
        result = parse_list(obj)
    elif isinstance(obj, dict):
        result = parse_dict(obj)
    else:
        # if string
        result = cgi.escape(obj).encode('ascii', 'xmlcharrefreplace')
    return result


def test_wrapper_1():
    assert html_wrapper('test', 'div#t1.cls1.cls2') == '<div id="t1" class="cls1 cls2">test</div>'


def test_wrapper_2():
    assert html_wrapper('test', 'div#t1') == '<div id="t1">test</div>'


def test_wrapper_3():
    assert html_wrapper('test', 'div.cls1.cls2') == '<div class="cls1 cls2">test</div>'


def test_wrapper_4():
    assert html_wrapper('test', 'div.cls1#t1.cls2') == '<div id="t1" class="cls1 cls2">test</div>'


def test_wrapper_5():
    assert html_wrapper('test', 'div') == '<div>test</div>'


def test_parse_dict():
    d = OrderedDict()
    d['h3'] = "Head #1"
    d['h2'] = 'Head #2'
    assert parse_dict(d) == '<h3>Head #1</h3><h2>Head #2</h2>'


def test_parse_list():
    d = {'h1': 'text'}
    assert parse_list([d]) == '<ul><li><h1>text</h1></li></ul>'


def test_parse_json():
    d = {'h1': 'text'}
    jsn = [
        {'l': [d]}
    ]
    assert parse_json(jsn) == '<ul><li><l><ul><li><h1>text</h1></li></ul></l></li></ul>'


def test_parse_json_1():
    assert parse_json({'h1': 'test'}) == '<h1>test</h1>'


def test_parse_json_2():
    assert parse_json({'h1': [{'h2': 'test'}]}) == '<h1><ul><li><h2>test</h2></li></ul></h1>'


if __name__ == '__main__':
    with open('source.json') as f:
        try:
            file_content = json.loads(f.read(), object_pairs_hook=OrderedDict)
            print parse_json(file_content)
        except ValueError:
            print "Incorrect JSON file"
